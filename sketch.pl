solution(R1,R2,R3,R4,R5) :-
   region(R1), region(R2), region(R3), region(R4), region(R5), 
   % Size constraints
      \+ small(R1), \+ small(R2), \+ small(R3), 
      \+ small(R4), small(R5),
   % Regularity constraints (none for R4)
       regular(R3), regular(R5), \+ regular(R2), \+ regular(R1),
   % Border constraints
       border(R1,R2), border(R2,R4),
   % Containment constraints
       inside(R3,R2), inside(R5,R4).

% The definitions of region, small, border, etc. are elsewhere.
