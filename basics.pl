%  This Prolog program does nothing of interest.

myname('Sydney J. Hurtubise').

:- nl, write('---------  Hello world.  My name is '), myname(X),
      write(X), write('!  ---------'), nl, nl.

