% A logic puzzle involving jobs and musical instruments,
solution(Flute) :-

   % Distinct occupations and instruments
   uniq_people(Doctor,Lawyer,Engineer),
   uniq_people(Piano,Violin,Flute),

   % The four clues
   \+ chris = Doctor,     % Chris is married to the doctor.
   Lawyer = Piano,        % The lawyer plays the piano.
   \+ Engineer = chris,   % The engineer is not Chris.
   Violin = Doctor,       % Sandy is a patient of 
   \+ sandy = Violin.     %    the violinist.

% uniq(...) is used to generate three distinct people.
uniq_people(A,B,C) :- person(A),  person(B),  person(C),  
                      \+ A=B, \+ A=C, \+ B=C. 

% The three given people
person(chris).   person(sandy).   person(pat).
