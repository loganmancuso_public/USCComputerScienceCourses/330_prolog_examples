This archive contains 55 files: this README.txt, the files applescript.txt,
basics.pdf, basics.pl, wordUtils.pl, and 50 example Prolog files.

See Appendix A of the book "Thinking as Computation" for a complete list of
these files and instructions regarding their use.
