%  This is the Prolog definition of the game Race to 21.

player(max).  player(min).        % The two players.

initial_state(21,max).            % Player Max goes first.

game_over(0,max,min).             % If it is Max's turn to play, 
game_over(0,min,max).             % then Min wins, and vice versa.

legal_move(OldState,_,Move,NewState) :- 
   small_number(Move),            % A move is a small number.
   OldState >= Move,
   NewState is OldState - Move.    

small_number(1).                  % A small number is either
small_number(2).                  % the number 1 or 2.
