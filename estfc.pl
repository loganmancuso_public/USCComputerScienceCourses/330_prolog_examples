% estfc(K,Q) holds if query Q can be established from knowledge base K.
% The method is forward-chaining.
estfc(K,Q) :- allsolved(K,Q).
estfc(K,Q) :- nkb(K,K1), estfc(K1,Q).

allsolved(_,[]).
allsolved(K,[A|Q]) :- solved(K,A), allsolved(K,Q).

solved(K,A) :- member([A],K).

nkb(K,[[A]|K]) :- member([A|B],K), \+ solved(K,A), allsolved(K,B).
