calc_arr(V,V1,V2,V3) :- 
   img_arr(V,X,V2,Y), ltv_seq(V,X,V1), rtv_seq(V,Y,V3).
calc_arr(V,V1,V2,occ) :- img_tv(V3,_,V,_), calc_arr(V,V1,V2,V3).
calc_arr(V,occ,V2,V3) :- img_tv(V1,_,V,_), calc_arr(V,V1,V2,V3).

calc_lv(V,V1,V2) :- 
   img_lv(V,X,Y), ltv_seq(V,X,V1), rtv_seq(V,Y,V2).
calc_lv(V,V1,occ) :- img_tv(V2,_,V,_), calc_lv(V,V1,V2).
calc_lv(V,occ,V2) :- img_tv(V1,_,V,_), calc_lv(V,V1,V2).
calc_lv(occ,V1,V2) :- img_tv(_,_,V1,_), img_tv(_,_,V2,_), \+ V1=V2.

% ltv_seq(V,X,Y): V X ... Y via T-vertices left to right
ltv_seq(_,X,X).
ltv_seq(V,X,Y) :- img_tv(X,V,_,Z), ltv_seq(X,Z,Y).

% rtv_seq(V,X,Y): V X ... Y via T-vertices right to left
rtv_seq(_,X,X).
rtv_seq(V,X,Y) :- img_tv(X,Z,_,V), rtv_seq(X,Z,Y).
