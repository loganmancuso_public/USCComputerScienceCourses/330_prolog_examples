% An example knowledge base about animals
background([
  [animal(X),dog(X)],  % Dogs are animals.
  [animal(X),cat(X)],  % Cats are animals.
  [animal(X),duck(X)], % Ducks are animals.
  % Some example animals
  [dog(fido)],    [dog(spot)],   [dog(rover)],
  [cat(kitty)],   [cat(kelly)], 
  [duck(donald)], [duck(daffy)], [duck(huey)] ]).

% Some predicates to learn about
predicate(animal(X),animal,[X]).   predicate(dog(X),dog,[X]). 
predicate(cat(X),cat,[X]).         predicate(duck(X),duck,[X]). 
predicate(barks(X),barks,[X]).     predicate(quacks(X),quacks,[X]). 
predicate(four_legged(X),four_legged,[X]). 
