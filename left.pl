% A new version of the left(X,Y) predicate.
% The rest of the program (for on, just_left, etc.) is elsewhere.
left(X,Y) :- base(X1,X), base(Y1,Y), table_left(X1,Y1).

% base(Z,X) holds if Z is on the table, below or equal to X.
base(X,X) :- \+ on(X,Y).                % X is not on anything.
base(Z,X) :- on(X,Y), base(Z,Y).        % X is on a block Y.

% table_left(X,Y): X and Y are on the table, X to the left of Y.
table_left(X,Y) :- just_left(X,Y).
table_left(X,Y) :- just_left(X,Z), table_left(Z,Y).
