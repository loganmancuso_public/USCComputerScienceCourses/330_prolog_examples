% This program solves a classroom scheduling problem.
solution(P1,P2,P3,P4,P5) :-  
  uniq_periods(P1,P2,P3,P4,P5),       % All different periods.
  not_2_in_a_row(P1,P2,P3,P4,P5),     % Not two consecutive hrs. 
  not_3_same_day(P1,P2,P3,P4,P5).     % Not three on the same day.

not_2_in_a_row(P1,P2,P3,P4,P5) :-
  \+ seq(P1,P2), \+ seq(P1,P3), \+ seq(P1,P4), \+ seq(P1,P5),
  \+ seq(P2,P3), \+ seq(P2,P4), \+ seq(P2,P5), \+ seq(P3,P4),
  \+ seq(P3,P5), \+ seq(P4,P5).

seq(A,B) :- A =:= B-1.
seq(A,B) :- A =:= B+1.

not_3_same_day(P1,P2,P3,P4,P5) :-
  \+ eqday(P1,P2,P3), \+ eqday(P1,P2,P4), \+ eqday(P1,P2,P5),
  \+ eqday(P1,P3,P4), \+ eqday(P1,P3,P5), \+ eqday(P1,P4,P5),
  \+ eqday(P2,P3,P4), \+ eqday(P2,P3,P5), \+ eqday(P2,P4,P5),
  \+ eqday(P3,P4,P5).

eqday(A,B,C) :- Z is A // 100, Z is B // 100, Z is C // 100.

% The definition of uniq_periods is elsewhere.
