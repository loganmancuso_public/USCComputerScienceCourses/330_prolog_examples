article(a).  article(the).  

common_noun(park,X) :- park(X).  
common_noun(tree,X) :- tree(X).
common_noun(hat,X) :- hat(X).  
common_noun(man,X) :- person(X), sex(X,male). 
common_noun(woman,X) :- person(X), sex(X,female). 

adjective(big,X) :- size(X,big).    
adjective(small,X) :- size(X,small). 
adjective(red,X) :- color(X,red).  
adjective(blue,X) :- color(X,blue). 

preposition(on,X,Y) :- on(X,Y).     
preposition(in,X,Y) :- in(X,Y). 
preposition(beside,X,Y) :- beside(X,Y). 
% The preposition 'with' is flexible in how it is used.
preposition(with,X,Y) :- on(Y,X).        % Y can be on X
preposition(with,X,Y) :- in(Y,X).        % Y can be in X
preposition(with,X,Y) :- beside(Y,X).    % Y can be beside X

% Any word that is not in one of the four categories above.
proper_noun(X,X) :- \+ article(X), \+ adjective(X,_), 
                    \+ common_noun(X,_), \+ preposition(X,_,_).
