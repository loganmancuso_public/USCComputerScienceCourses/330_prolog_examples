% A 3D view of a cuboid (seven vertices)
cuboid(A,B,C,D,E,F,G) :-  
   calc_arr(A,F,G,B), calc_lv(B,A,C), calc_arr(C,B,G,D), 
   calc_lv(D,C,E), calc_arr(E,D,G,F), calc_lv(F,E,A), 
   calc_fork(G,A,C,E).

% Vertex V, then left, mid, and right vertices
calc_arr(V,V1,V2,V3) :- img_arr(V,V1,V2,V3).

% Vertex V, then left and right vertices
calc_lv(V,V1,V2) :- img_lv(V,V1,V2).

% Vertex V, then the vertices in clockwise order
calc_fork(V,V1,V2,V3) :- img_fork(V,V1,V2,V3).
calc_fork(V,V1,V2,V3) :- img_fork(V,V3,V1,V2).
calc_fork(V,V1,V2,V3) :- img_fork(V,V2,V3,V1).
