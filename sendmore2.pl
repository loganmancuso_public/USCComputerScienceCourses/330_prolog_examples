% solution(...) holds for a solution to SEND+MORE=MONEY.
solution(S,E,N,D,M,O,R,Y) :- 
   dig(D), dig(E), 
   Y is (D+E) mod 10, C1 is (D+E) // 10, 
   dig(N), dig(R), 
   E is (N+R+C1) mod 10, C10 is (N+R+C1) // 10,
   dig(E), dig(O), 
   N is (E+O+C10) mod 10, C100 is (E+O+C10) // 10,
   dig(S), S > 0, dig(M), M > 0, 
   O is (S+M+C100) mod 10, M is (S+M+C100) // 10,
   uniq_digits(S,E,N,D,M,O,R,Y).

% The rest of the program is as before.
