simple_declarative(String) :- 
   split_words(String,Words),           % Get words from string.
   sd(Words).                           % Use sd on the words.

sd(Words) :-
   append(NP1,[is,Prep|NP2],Words),     % Split words.
   np(NP1,X),                           % Find referent for first NP.
   np(NP2,Y),                           % Find referent for second NP.
   add_for_preposition(Prep,X,Y).       % Add new atom to database.
